project_id = "shared-vpc-sandbox-325923"
name = "testing-routes"
subnets = [{
        ip_cidr_range = "192.168.0.0/16"
        name = "subnet-1"
        region = "us-east1"
        secondary_ip_range = {}
    }]

routes = {
    private-googleapis-route = {
        dest_range = "199.36.153.8/30"
        next_hop_type = "gateway"
        next_hop = "default-internet-gateway"
        priority = 0
        tags = []
    }
}

delete_default_routes_on_create = true