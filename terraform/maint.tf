module "vpc" {
  source     = "./modules/net-vpc"
  project_id = var.project_id
  name       = "packer-network"
  subnets = [
    {
      ip_cidr_range      = "10.20.0.0/24"
      name               = "production"
      region             = "us-central1"
      secondary_ip_range = {}
    },
  ]
}

data "google_compute_image" "my_latest_image" {
  family  = "packer-test"
  project = var.project_id
}

module "simple-vm-example" {
  source     = "./modules/compute-vm"
  project_id = var.project_id
  zone       = "us-central1-b"
  name       = "test"
  network_interfaces = [{
    network    = module.vpc.network.id
    subnetwork = module.vpc.subnet_self_links["us-central1/production"]
    nat        = true
    addresses = {
      external = ""
      internal = ""
    }
  }]
  boot_disk = {
    image = data.google_compute_image.my_latest_image.id
    size  = 50
    type  = "pd-ssd"
  }
  service_account_create = true
}
