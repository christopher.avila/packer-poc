variable "project_id" {
    type = string
}

variable "packer_username" {
    type = string
}

variable "subnetwork" {
    type = string
}

variable "windows_image" {
    type = string
}

variable "user" {
    type = string
}
