packer {
  required_plugins {
    googlecompute = {
      version = " >= 0.0.1"
      source = "github.com/hashicorp/googlecompute"
    }
  }
}

source "googlecompute" "windows-example" {
  project_id = var.project_id
  subnetwork = var.subnetwork
  source_image = var.windows_image
  zone = "us-central1-b"
  disk_size = 50
  machine_type = "n1-standard-2"
  communicator = "winrm"
  image_name = "packer-test-${formatdate("YYYYMMDDhhmmss", timestamp())}"
  image_family = "packer-test"
  image_description = "This is a test."
  winrm_username = var.user
  winrm_insecure = true
  winrm_use_ssl = true
  metadata = {
    sysprep-specialize-script-cmd = "winrm quickconfig -quiet & net user /add ${var.user} & net localgroup administrators packer_user /add & winrm set winrm/config/service/auth @{Basic=\"true\"}"
  }
}

build {
  sources = ["sources.googlecompute.windows-example"]

  provisioner "ansible" {
    use_proxy = false
    user = var.user
    extra_arguments = [
      "--extra-vars", "winrm_password=${build.Password}", 
      "--extra-vars", "ansible_winrm_server_cert_validation=ignore"]
    playbook_file = "./playbook.yml"
  }
}

